import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ProcessamentoImagem {
	
	public static void main(String[] args)throws IOException {

	 File f = null;
	 f = new File("D:\\Arquivos\\-downloads\\fotos\\demon.jpg");
	 BufferedImage image = ImageIO.read(f);
	 BufferedImage imagemsaidaR = bandaR(image);
	 BufferedImage imagemsaidaB = bandaB(image);
	 BufferedImage imagemsaidaG = bandaG(image);
	 
	 System.out.println("até aqui deu certo.");
	 
		
	 int altura = image.getHeight();
	 int largura = image.getWidth();
	 System.out.printf("\nlargura = %d", largura);
	 System.out.printf("\naltura = %d", altura);
	 
	 Color azul = new Color (0,0,255);
	 Color verde = new Color (0,255,0);
	 Color vermelho = new Color (255,0,0);
	
	 image.setRGB(0, 0, azul.getRGB());
	 image.setRGB(largura/2, altura/2, verde.getRGB());
	 image.setRGB(largura-1, altura-1, vermelho.getRGB());

	
	 System.out.println("deu certo.");
	 
	 for (int lin = 0; lin < altura; lin++) {
	 	for (int col = 0; col < largura; col++) {
	 		Color C = new Color(image.getRGB(col,lin));
	 		int R = C.getRed();
	 		int G = C.getGreen();
	 		int B = C.getBlue();
	 		//System.out.println("[LINHA: " + lin + "] [COLUNA: " + col +"] Vermelho: " + R + " Verde: " + G + " Azul: " + B);

		  }
		}
	 
	 ImageIO.write(image, "jpg", new File("D:\\Arquivos\\-downloads\\fotos\\demon-clone.jpg"));
	 System.out.println("deu certo aqui tb");
	 
	

	 
	 JLabel mylabel = new JLabel(new ImageIcon(image));
	 JPanel mypanel = new JPanel();
	 mypanel.add(mylabel);
	 JFrame myframe = new JFrame();
	 myframe.setSize(new Dimension(largura, altura));
	 myframe.add(mypanel);
	 myframe.setVisible(true);
	 
	 //exibindo imagem vermelho
	 JLabel mylabel1 = new JLabel(new ImageIcon(imagemsaidaB));
	 JPanel mypanel1 = new JPanel();
	 mypanel1.add(mylabel1);
	 JFrame myframe1 = new JFrame();
	 myframe1.setSize(new Dimension(largura, altura));
	 myframe1.add(mypanel1);
	 myframe1.setVisible(true);
	 
	 
	 //exibindo imagem azul
	 JLabel mylabel2 = new JLabel(new ImageIcon(imagemsaidaR));
	 JPanel mypanel2 = new JPanel();
	 mypanel2.add(mylabel2);
	 JFrame myframe2 = new JFrame();
	 myframe2.setSize(new Dimension(largura, altura));
	 myframe2.add(mypanel2);
	 myframe2.setVisible(true);
	 
	 //exibindo imagem verde
	 JLabel mylabel3 = new JLabel(new ImageIcon(imagemsaidaG));
	 JPanel mypanel3 = new JPanel();
	 mypanel3.add(mylabel3);
	 JFrame myframe3 = new JFrame();
	 myframe3.setSize(new Dimension(largura, altura));
	 myframe3.add(mypanel3);
	 myframe3.setVisible(true);
	 

	 

	}
	
	public static BufferedImage bandaR (BufferedImage image) {
		 int altura = image.getHeight();
		 int largura = image.getWidth();
		 BufferedImage imagemsaidaR = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		 for (int lin = 0; lin < altura; lin++) {
			 	for (int col = 0; col < largura; col++) {
			 		int rgb = image.getRGB(col, lin);
			 		Color cor = new Color(rgb);
			 		int red = cor.getRed();
			 		Color vermelhoApenas = new Color(red, 0, 0);
			 		imagemsaidaR.setRGB(col, lin, vermelhoApenas.getRGB());
				 }
		}
		 
		 return imagemsaidaR;
	}
	
	
	public static BufferedImage bandaB (BufferedImage image) {
		 int altura = image.getHeight();
		 int largura = image.getWidth();
		 BufferedImage imagemsaidaB = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		 
		 for (int lin = 0; lin < altura; lin++) {
			 	for (int col = 0; col < largura; col++) {
			 		int rgb = image.getRGB(col, lin);
			 		Color cor = new Color(rgb);
			 		int azul = cor.getBlue();
			 		Color azulApenas = new Color(0, 0, azul);
			 		imagemsaidaB.setRGB(col, lin, azulApenas.getRGB());
				 }
		}
		 
		 return imagemsaidaB;
	}
	
	public static BufferedImage bandaG (BufferedImage image) {
		 int altura = image.getHeight();
		 int largura = image.getWidth();
		 BufferedImage imagemsaidaG = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		 
		 for (int lin = 0; lin < altura; lin++) {
			 	for (int col = 0; col < largura; col++) {
			 		int rgb = image.getRGB(col, lin);
			 		Color cor = new Color(rgb);
			 		int verde = cor.getBlue();
			 		Color azulApenas = new Color(0, verde, 0);
			 		imagemsaidaG.setRGB(col, lin, azulApenas.getRGB());
				 }
		}
		 
		 return imagemsaidaG;
	}



}
